import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
   application
   kotlin("jvm") version "1.6.0"
}

application {
   mainClass.set("util.Runner")
}

version = "1.0-SNAPSHOT"

repositories {
   mavenCentral()
   // jcenter()
}

dependencies {
   implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.0")
   // implementation("org.jetbrains.kotlinx", "kotlinx-collections-immutable-jvm", "0.3.3")
   implementation("org.reflections", "reflections", "0.9.12")
   testImplementation("org.hamcrest", "hamcrest", "2.2")
   testImplementation("org.junit.jupiter", "junit-jupiter", "5.8.2")
}

tasks.withType<KotlinCompile> {
   kotlinOptions.jvmTarget = "17"
}

tasks.named<Test>("test") {
   useJUnitPlatform()
   testLogging {
      events("passed", "skipped", "failed")
   }
}
