package days

import kotlin.math.abs

class Day07 : Day(7) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    private fun firstCostToAlignOnPoint(destination: Int, positions: Map<Int, Int>): Int =
        positions.map { e -> abs(e.key - destination) * e.value }.sum()

    private fun secondCostToAlignOnPoint(destination: Int, positions: Map<Int, Int>): Int =
        positions
            .map { e ->
                val distance = abs(e.key - destination)
                val costToMove = distance * (distance + 1) / 2 // equal to (1 .. distance).sum()
                costToMove * e.value
            }
            .sum()

    fun solvePartOne(inputList: List<String>): Int {
        // key: position, value: count
        val initialPositions = inputList[0].split(",").map { it.toInt() }.sorted()
        val groupedInitialPositions = initialPositions
            .groupingBy { it }
            .eachCount()
        val median = initialPositions.let { (it[it.size / 2] + it[(it.size - 1) / 2]) / 2 }
        val searchRange = (median - 1..median + 1)
        return searchRange
            .map { Pair(it, firstCostToAlignOnPoint(it, groupedInitialPositions)) }
            .minByOrNull { it.second }
            ?.second ?: TODO()
        // The solution for example and real input for part 1 is the median,
        // but I'm not sure if that isn't just a quirk of the input.
        // Some inputs might require a more thorough search
    }

    fun solvePartTwo(inputList: List<String>): Int {
        // key: position, value: count
        val initialPositions = inputList[0].split(",").map { it.toInt() }
        val groupedInitialPositions = initialPositions
            .groupingBy { it }
            .eachCount()
        val average = initialPositions.average().toInt()
        val searchRange = (average - 1..average + 1)
        return searchRange
            .map { Pair(it, secondCostToAlignOnPoint(it, groupedInitialPositions)) }
            .minByOrNull { it.second }
            ?.second ?: TODO()
        // The solution for example and real input for part 2 is the average (rounded to integer)
        // but I'm not sure if that isn't just a quirk of the input.
        // Some inputs might require a more thorough search
    }
}