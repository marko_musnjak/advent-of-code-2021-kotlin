package days

class Day24 : Day(24) {
    private enum class Instruction(val text: String) {
        INP("inp"),
        ADD("add"),
        MUL("mul"),
        DIV("div"),
        MOD("mod"),
        EQL("eql");

        companion object {
            fun fromText(txt: String): Instruction = when (txt) {
                "inp" -> INP
                "add" -> ADD
                "mul" -> MUL
                "div" -> DIV
                "mod" -> MOD
                "eql" -> EQL
                else -> TODO()
            }
        }
    }

    private enum class Register() {
        W,
        X,
        Y,
        Z;

        companion object {
            fun fromText(txt: String): Register = when (txt) {
                "w" -> W
                "x" -> X
                "y" -> Y
                "z" -> Z
                else -> TODO()
            }
        }
    }

    private sealed class Operation(val instruction: Instruction, val register: Register) {
        abstract fun applyToState(state: ComputerState): ComputerState

        companion object {
            fun fromString(str: String): Operation {
                val parts = str.split(" ")
                val instruction = Instruction.fromText(parts[0])
                val register = Register.fromText(parts[1])
                return when (instruction) {
                    Instruction.INP -> Input(register)
                    else -> {
                        val lastParameter = parts[2]
                        if (listOf("w", "x", "y", "z").contains(lastParameter)) {
                            val otherRegister = Register.fromText(lastParameter)
                            when (instruction) {
                                Instruction.ADD -> AddRegister(register, otherRegister)
                                Instruction.MUL -> MulRegister(register, otherRegister)
                                Instruction.DIV -> DivRegister(register, otherRegister)
                                Instruction.MOD -> ModRegister(register, otherRegister)
                                Instruction.EQL -> EqlRegister(register, otherRegister)
                                Instruction.INP -> TODO()
                            }
                        } else {
                            val literal = lastParameter.toLong()
                            when (instruction) {
                                Instruction.ADD -> AddLiteral(register, literal)
                                Instruction.MUL -> MulLiteral(register, literal)
                                Instruction.DIV -> DivLiteral(register, literal)
                                Instruction.MOD -> ModLiteral(register, literal)
                                Instruction.EQL -> EqlLiteral(register, literal)
                                Instruction.INP -> TODO()
                            }
                        }
                    }
                }
            }
        }
    }

    private class Input(register: Register) : Operation(Instruction.INP, register) {
        override fun applyToState(state: ComputerState): ComputerState =
            state.withRegister(register, state.inputQueue.first()!!)
                .withInputQueue(state.inputQueue.drop(1))
    }

    private abstract class Add(register: Register) : Operation(Instruction.ADD, register)

    private class AddLiteral(register: Register, val literal: Long) : Add(register) {
        override fun applyToState(state: ComputerState) =
            state.withRegister(register, state.getValueInRegister(register) + literal)
    }

    private class AddRegister(register: Register, val other: Register) : Add(register) {
        override fun applyToState(state: ComputerState): ComputerState =
            state.withRegister(register, state.getValueInRegister(register) + state.getValueInRegister(other))
    }

    private abstract class Mul(register: Register) : Operation(Instruction.MUL, register)
    private class MulLiteral(register: Register, val literal: Long) : Mul(register) {
        override fun applyToState(state: ComputerState) =
            state.withRegister(register, state.getValueInRegister(register) * literal)
    }

    private class MulRegister(register: Register, val other: Register) : Mul(register) {
        override fun applyToState(state: ComputerState): ComputerState =
            state.withRegister(register, state.getValueInRegister(register) * state.getValueInRegister(other))
    }

    private abstract class Div(register: Register) : Operation(Instruction.DIV, register)
    private class DivLiteral(register: Register, val literal: Long) : Div(register) {
        override fun applyToState(state: ComputerState) =
            state.withRegister(register, state.getValueInRegister(register) / literal)
    }

    private class DivRegister(register: Register, val other: Register) : Div(register) {
        override fun applyToState(state: ComputerState): ComputerState =
            state.withRegister(register, state.getValueInRegister(register) / state.getValueInRegister(other))
    }

    private abstract class Mod(register: Register) : Operation(Instruction.MOD, register)
    private class ModLiteral(register: Register, val literal: Long) : Mod(register) {
        override fun applyToState(state: ComputerState) =
            state.withRegister(register, state.getValueInRegister(register) % literal)
    }

    private class ModRegister(register: Register, val other: Register) : Mod(register) {
        override fun applyToState(state: ComputerState): ComputerState =
            state.withRegister(register, state.getValueInRegister(register) % state.getValueInRegister(other))
    }

    private abstract class Eql(register: Register) : Operation(Instruction.EQL, register)
    private class EqlLiteral(register: Register, val literal: Long) : Eql(register) {
        override fun applyToState(state: ComputerState): ComputerState =
            state.withRegister(register, if (state.getValueInRegister(register) == literal) 1L else 0L)
    }

    private class EqlRegister(register: Register, val other: Register) : Eql(register) {
        override fun applyToState(state: ComputerState): ComputerState =
            state.withRegister(
                register,
                if (state.getValueInRegister(register) == state.getValueInRegister(other)) 1L else 0L
            )
    }

    private data class ComputerState(
        val w: Long,
        val x: Long,
        val y: Long,
        val z: Long,
        val inputQueue: List<Long>,
    ) {
        fun withRegister(register: Register, value: Long): ComputerState = when (register) {
            Register.W -> ComputerState(value, this.x, this.y, this.z, this.inputQueue)
            Register.X -> ComputerState(this.w, value, this.y, this.z, this.inputQueue)
            Register.Y -> ComputerState(this.w, this.x, value, this.z, this.inputQueue)
            Register.Z -> ComputerState(this.w, this.x, this.y, value, this.inputQueue)
        }

        fun withInputQueue(newQueue: List<Long>) = ComputerState(this.w, this.x, this.y, this.z, newQueue)

        fun getValueInRegister(register: Register): Long = when (register) {
            Register.W -> this.w
            Register.X -> this.x
            Register.Y -> this.y
            Register.Z -> this.z
        }

        companion object {
            fun resetWithInput(input: List<Long>) = ComputerState(0, 0, 0, 0, input)
        }
    }

    override fun partOne(): Long {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Long {
        return solvePartTwo(inputList)
    }

    private tailrec fun searchBetween(lower: Long, upper: Long, program: List<Operation>): Long {
        println("Searching between $lower and $upper")
        if (lower == upper) return lower
        // if any digit in lower is 0, find first such, set it and all after it to 1
        val newLower = lower.toString()
            .fold(Pair(mutableListOf<Char>(), false)) { acc, digit ->
                val newSeen = acc.second || digit == '0'
                acc.first.add(if (newSeen) '1' else digit)
                Pair(acc.first, newSeen)
            }
            .first.joinToString("").toLong()

        // if any digit in upper is 0, find first such, all after it to 0, subtract 1 in the end
        val newUpperModifier = upper.toString()
            .fold(Pair(mutableListOf<Char>(), false)) { acc, digit ->
                val newSeen = acc.second || digit == '0'
                acc.first.add(if (newSeen) '0' else digit)
                Pair(acc.first, newSeen)
            }
        val newUpper = newUpperModifier.first.joinToString("").toLong() - if (newUpperModifier.second) 1L else 0L
        if (newLower != lower || newUpper != upper) println("Adjusting bounds to search between $newLower and $newUpper")
        val midpoint = ((newLower + newUpper) / 2).toString().toList().map { it.code.toLong() - '0'.code.toLong() }
        println("Testing value $midpoint")
        val accepted = program.fold(ComputerState.resetWithInput(midpoint)) { s, o -> o.applyToState(s) }.z == 0L
        println("Accepted: $accepted")
        return if (accepted) searchBetween((newLower + newUpper) / 2L - 1L, newUpper, program) else searchBetween(
            newLower,
            (newLower + newUpper) / 2L + 1L,
            program
        )
    }

    fun solvePartOne(input: List<String>): Long {
        val program = input.map { Operation.fromString(it) }
        val a = program.fold(ComputerState.resetWithInput("69914999975369".toList().map { it.code.toLong() - '0'.code.toLong() })) {s, o -> o.applyToState(s)}
        return 1L
        // return searchBetween(11_111_111_111_111, 99_999_999_999_999, program)
    }

    fun solvePartTwo(input: List<String>): Long {
        return 0
    }

}
