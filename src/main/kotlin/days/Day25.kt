package days

class Day25 : Day(25) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(input: List<String>): Int {
        val initialPosition = input.map { it.toCharArray() }.toTypedArray()
        val rows = initialPosition.size
        val cols = initialPosition[0].size
        var currentStep = 0
        val canMove = Array(rows) { BooleanArray(cols) }

        fun oneMove(c: Char, rowStep: Int, colStep: Int): Boolean {
            var moved = false
            for (row in 0 until rows) for (col in 0 until cols) {
                canMove[row][col] =
                    initialPosition[row][col] == c && initialPosition[(row + rowStep) % rows][(col + colStep) % cols] == '.'
                if (canMove[row][col]) moved = true
            }
            for (row in 0 until rows) for (col in 0 until cols) if (canMove[row][col]) {
                initialPosition[row][col] = '.'
                initialPosition[(row + rowStep) % rows][(col + colStep) % cols] = c
            }
            return moved
        }

        do {
            currentStep++
            val moved1 = oneMove('>', 0, 1)
            val moved2 = oneMove('v', 1, 0)
        } while (moved1 || moved2)

        return currentStep
    }

    fun solvePartTwo(input: List<String>): Int {
        return 0
    }

}