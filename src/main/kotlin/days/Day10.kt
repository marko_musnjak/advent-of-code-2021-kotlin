package days

import kotlin.collections.ArrayDeque

data class LineValidationState(val valid: Boolean, val queue: ArrayDeque<Char>)

class Day10 : Day(10) {
    private val partOneValueMap = mapOf(
        ')' to 3,
        ']' to 57,
        '}' to 1197,
        '>' to 25137,
        ' ' to 0
    )

    private val partTwoValueMap = mapOf(
        ')' to 1L,
        ']' to 2L,
        '}' to 3L,
        '>' to 4L,
        ' ' to 0L
    )

    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Long {
        return solvePartTwo(inputList)
    }

    private fun isOpening(c: Char): Boolean = c == '(' || c == '[' || c == '{' || c == '<'
    private fun opposing(c: Char): Char = when (c) {
        '(' -> ')'
        '[' -> ']'
        '{' -> '}'
        '<' -> '>'
        else -> ' '
    }

    private fun validateLine(line: String): LineValidationState {
        return line.fold(LineValidationState(true, ArrayDeque())) { acc, c ->
            if (!acc.valid) acc else {
                if (isOpening(c)) {
                    acc.queue.addLast(opposing(c))
                    LineValidationState(true, acc.queue)
                } else {
                    val expected = acc.queue.last()
                    if (c == expected) {
                        acc.queue.removeLast()
                        LineValidationState(true, acc.queue)
                    } else {
                        acc.queue.addLast(c) // we need the mismatched one, not the expected one
                        LineValidationState(false, acc.queue)
                    }
                }
            }
        }
    }

    fun solvePartOne(input: List<String>): Int {
        return input
            .map { validateLine(it) }
            .filter { !it.valid }
            .map { it.queue.last() }
            .sumOf { partOneValueMap.getOrDefault(it, 0) }
    }

    fun solvePartTwo(input: List<String>): Long {
        val sums = input
            .map { validateLine(it) }
            .filter { it.valid }
            .map { line ->
                line.queue.reversed().fold(0L) { acc, c ->
                    acc * 5L + partTwoValueMap.getOrDefault(c, 0L)
                }
            }
            .sorted()
        return sums[sums.size / 2]
    }
}