package days

class Day20 : Day(20) {
    data class Point(val row: Int, val col: Int)

    companion object {
        private const val DARK = false
        private const val LIGHT = true
    }

    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    private fun getLookupMap(input: List<String>) = input[0].map {
        when (it) {
            '#' -> LIGHT
            '.' -> DARK
            else -> throw RuntimeException("Invalid input")
        }
    }

    private fun getInitialImage(input: List<String>) = input.drop(2)
        .flatMapIndexed { lineIndex, line ->
            line.mapIndexedNotNull { colIndex, char ->
                if (char == '#') Point(lineIndex, colIndex) else null
            }
        }
        .toSet()

    fun solvePartOne(input: List<String>): Int {
        val lookupMap = getLookupMap(input)
        val initialImage = getInitialImage(input)

        val finalImage = (1..2).fold(Pair(initialImage, false)) { acc, counter ->
            Pair(applyAlgorithm(acc.first, lookupMap, lookupMap[0] and ((counter % 2) == 0)), !acc.second)
        }
        return finalImage.first.size
    }

    fun solvePartTwo(input: List<String>): Int {
        val lookupMap = getLookupMap(input)
        val initialImage = getInitialImage(input)

        val finalImage = (1..50).fold(Pair(initialImage, false)) { acc, counter ->
            Pair(applyAlgorithm(acc.first, lookupMap, lookupMap[0] and ((counter % 2) == 0)), !acc.second)
        }
        return finalImage.first.size
    }

    private fun getBit(
        row: Int,
        col: Int,
        rowRange: IntRange,
        colRange: IntRange,
        image: Set<Point>,
        outsideValue: Boolean
    ) = if (row !in rowRange || col !in colRange) outsideValue else image.contains(Point(row, col))


    private fun applyAlgorithm(image: Set<Point>, decodingKey: List<Boolean>, evenRound: Boolean): Set<Point> {

        val border = 1
        val minRow = (image.minByOrNull { it.row }?.row ?: 0)
        val maxRow = (image.maxByOrNull { it.row }?.row ?: 0)

        val minCol = (image.minByOrNull { it.col }?.col ?: 0)
        val maxCol = (image.maxByOrNull { it.col }?.col ?: 0)

        val rowRange = (minRow..maxRow)
        val colRange = (minCol..maxCol)

        return (minRow - border..maxRow + border).flatMap { row ->
            (minCol - border..maxCol + border).mapNotNull { col ->
                val bits = Array(9) { false }
                bits[8] = getBit(row - 1, col - 1, rowRange, colRange, image, evenRound)
                bits[7] = getBit(row - 1, col, rowRange, colRange, image, evenRound)
                bits[6] = getBit(row - 1, col + 1, rowRange, colRange, image, evenRound)
                bits[5] = getBit(row, col - 1, rowRange, colRange, image, evenRound)
                bits[4] = getBit(row, col, rowRange, colRange, image, evenRound)
                bits[3] = getBit(row, col + 1, rowRange, colRange, image, evenRound)
                bits[2] = getBit(row + 1, col - 1, rowRange, colRange, image, evenRound)
                bits[1] = getBit(row + 1, col, rowRange, colRange, image, evenRound)
                bits[0] = getBit(row + 1, col + 1, rowRange, colRange, image, evenRound)

                val decodingIndex = 0 +
                        bits[8].toInt().shl(8) +
                        bits[7].toInt().shl(7) +
                        bits[6].toInt().shl(6) +
                        bits[5].toInt().shl(5) +
                        bits[4].toInt().shl(4) +
                        bits[3].toInt().shl(3) +
                        bits[2].toInt().shl(2) +
                        bits[1].toInt().shl(1) +
                        bits[0].toInt()
                if (decodingKey[decodingIndex]) Point(row, col) else null
            }
        }
            .toSet()
    }

    private fun printImage(image: Set<Point>, border: Int = 5): Unit {
        val minRow = (image.minByOrNull { it.row }?.row ?: 0) - border
        val maxRow = (image.maxByOrNull { it.row }?.row ?: 0) + border
        val minCol = (image.minByOrNull { it.col }?.col ?: 0) - border
        val maxCol = (image.maxByOrNull { it.col }?.col ?: 0) + border

        val decodedImage = (minRow..maxRow).joinToString("\n") { row ->
            (minCol..maxCol).joinToString("") { col ->
                when (image.contains(Point(row, col))) {
                    false -> "."
                    true -> "#"
                }
            }
        }
        println(decodedImage)
        println()
    }

}