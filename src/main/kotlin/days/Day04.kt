package days

class BingoBoard(private var numbers: List<Int>) {

    fun mark(number: Int) {
        numbers = numbers.map { if (it == number) it + MARKER else it }
    }

    private fun getColumn(col: Int): List<Int> {
        return (0..4).map { row -> numbers[row * 5 + col] }
    }

    fun check(): Boolean {
        val anyRowsMatching = numbers.windowed(5, 5).any { rowItems -> rowItems.all { it >= MARKER } }
        val anyColsMatching = (0..4).map { this.getColumn(it) }.any { col -> col.all { it >= MARKER } }
        return anyRowsMatching || anyColsMatching
    }

    fun score(): Int {
        return numbers.filter { it < MARKER }.sum()
    }

    companion object {
        const val MARKER = 100_000
    }
}

class Day04 : Day(4) {
    override fun partOne(): Int {
        return solvePartOne(inputString)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputString)
    }

    fun solvePartOne(inputString: String): Int {
        val inputData = inputString.split("\n\n")
        val bingoNumbers = inputData[0].split(",").map { it.toInt() }
        val boards = inputData
            .drop(1)
            .map { board ->
                board
                    .replace("\n", " ")
                    .replace("  ", " ")
                    .split(" ")
                    .filter { it.isNotEmpty() }
                    .map { num -> num.toInt() }
            }
            .map { BingoBoard(it) }

        bingoNumbers.forEach { num ->
            boards.forEach { board ->
                board.mark(num)
                if (board.check()) {
                    return board.score() * num
                }

            }
        }
        return 0
    }

    fun solvePartTwo(inputString: String): Int {
        val inputData = inputString.split("\n\n")
        val bingoNumbers = inputData[0].split(",").map { it.toInt() }
        val boards = inputData
            .drop(1)
            .map { board ->
                board
                    .replace("\n", " ")
                    .replace("  ", " ")
                    .split(" ")
                    .filter { it.isNotEmpty() }
                    .map { num -> num.toInt() }
            }
            .map { BingoBoard(it) }

        val winIndex = HashMap<Int, Int>()
        val completedBoards = HashSet<BingoBoard>()

        bingoNumbers.forEachIndexed { numberIndex, num ->
            boards.forEachIndexed { boardIndex, board ->
                if (!completedBoards.contains(board)) {
                    board.mark(num)
                    if (board.check()) {
                        winIndex[numberIndex] = boardIndex
                        completedBoards.add(board)
                    }
                }
            }
        }
        val winningBoardAndNum = winIndex.maxByOrNull { it.key }
        return boards[winningBoardAndNum!!.value].score() * bingoNumbers[winningBoardAndNum.key]
    }

}