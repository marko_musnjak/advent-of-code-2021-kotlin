package days

import kotlin.math.abs

class Day22 : Day(22) {
    override fun partOne(): Long {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Long {
        return solvePartTwo(inputList)
    }

    private data class Step(val op: String, val xRange: IntRange, val yRange: IntRange, val zRange: IntRange)

    fun solvePartOne(input: List<String>): Long {
        val steps = input.mapNotNull { line ->
            val firstSplit = line.split(" ")
            val secondSplit = firstSplit[1].split(",")
                .map { it.drop(2) }
                .map { it.split("..") }
                .map { it.map { t -> t.toInt() } }
                .filter { it.all { n -> abs(n) <= 50 } }
                .map { if (it[0] < it[1]) (it[0]..it[1]) else (it[1]..it[0]) }
            if (firstSplit.isEmpty() || secondSplit.isEmpty()) null else Step(
                firstSplit[0],
                secondSplit[0],
                secondSplit[1],
                secondSplit[2]
            )
        }
        return steps.fold(emptySet<Triple<Int, Int, Int>>()) { acc, step ->
            val newSet = acc.toMutableSet()
            val action =
                if (step.op == "on") MutableSet<Triple<Int, Int, Int>>::add else MutableSet<Triple<Int, Int, Int>>::remove
            step.xRange.forEach { x ->
                step.yRange.forEach { y ->
                    step.zRange.forEach { z ->
                        action(newSet, Triple(x, y, z))
                    }
                }
            }
            newSet
        }
            .size.toLong()
    }

    fun solvePartTwo(input: List<String>): Long {
        return 0
    }

}