package days

import kotlin.math.max
import kotlin.math.sqrt

class Day17 : Day(17) {
    override fun partOne(): Int {
        return solvePartOne(inputString)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputString)
    }

    data class Vector2D(val x: Int, val y: Int)

    private fun generateSteps(
        startingSpeedVector: Vector2D,
        lowerRight: Vector2D
    ): Sequence<Vector2D> {
        return sequence {
            var currentPosition = Vector2D(0, 0)
            var speedVector = Vector2D(startingSpeedVector.x, startingSpeedVector.y)
            while (true) {
                val dragEffect = if (speedVector.x == 0) 0 else if (speedVector.x < 0) 1 else -1
                currentPosition = Vector2D(currentPosition.x + speedVector.x, currentPosition.y + speedVector.y)
                speedVector = Vector2D(max(speedVector.x + dragEffect, 0), speedVector.y - 1)
                if (currentPosition.x > lowerRight.x || currentPosition.y < lowerRight.y) break
                yield(currentPosition)
            }
        }
    }

    fun solvePartOne(input: String): Int {
        val range = input.split(": ")[1]
            .split(", ")
            .map { it.drop(2) }
            .map { oneRange -> oneRange.split("..").map { it.toInt() } }

        // when flying back from highest point, we need to hit height 0 with speed to hit the lower end of the given range
        // the height that will be reached with that speed is the sum up to the lower end
        return range[1][0] * (range[1][0] + 1) / 2
    }

    fun solvePartTwo(input: String): Int {

        val range = input.split(": ")[1]
            .split(", ")
            .map { it.drop(2) }
            .map { oneRange -> oneRange.split("..").map { it.toInt() } }

        val minimumHorizontalSpeed = sqrt(2.0 * range[0].minOrNull()!!).toInt()
        val maxHorizontalSpeed = range[0][1]

        val lowerRight = Vector2D(range[0][1], range[1][0])

        return (minimumHorizontalSpeed..maxHorizontalSpeed)
            .flatMap { horizontalSpeed ->
                (range[1][0]..-range[1][0]).map { verticalSpeed ->
                    generateSteps(Vector2D(horizontalSpeed, verticalSpeed), lowerRight)
                        .dropWhile {
                            val xInBounds = ((range[0].minOrNull()!!)..(range[0].maxOrNull()!!)).contains(it.x)
                            val yInBounds = ((range[1].minOrNull()!!)..(range[1].maxOrNull()!!)).contains(it.y)
                            !xInBounds || !yInBounds
                        }
                        .any()
                }
            }
            .count { it }
    }
}
