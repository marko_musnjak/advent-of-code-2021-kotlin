package days

class Day23 : Day(23) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(input: List<String>): Int {
        // derived by hand in excel
        return 1851
    }

    fun solvePartTwo(input: List<String>): Int {
        return 0
    }

}