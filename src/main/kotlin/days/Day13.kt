package days

class Day13 : Day(13) {
    override fun partOne(): Int {
        return solvePartOne(inputString)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputString)
    }

    private fun foldAlongVerticalLine(
        sheet: Set<Pair<Int, Int>>,
        foldLine: Int
    ): Set<Pair<Int, Int>> {
        // x = foldLine
        return sheet
            .map { dot ->
                if (dot.first < foldLine) dot else {
                    val newX = foldLine - (dot.first - foldLine)
                    Pair(newX, dot.second)
                }
            }
            .toSet()
    }

    private fun foldAlongHorizontalLine(
        sheet: Set<Pair<Int, Int>>,
        foldLine: Int
    ): Set<Pair<Int, Int>> {
        // y = foldLine
        return sheet
            .map { dot ->
                if (dot.second < foldLine) dot else {
                    val newY = foldLine - (dot.second - foldLine)
                    Pair(dot.first, newY)
                }
            }
            .toSet()
    }

    private fun printSheet(sheet: Set<Pair<Int, Int>>): String {
        val sizeX = sheet.maxOfOrNull { it.first } ?: 0
        val sizeY = sheet.maxOfOrNull { it.second } ?: 0
        val display = (0..sizeY).joinToString("\n") { y ->
            (0..sizeX).map { x ->
                if (sheet.contains(Pair(x, y))) '#' else ' '
            }.joinToString("")
        }
        return display + "\n" + "=".repeat(sizeX + 2)
    }

    private fun parseInput(input: String): Pair<Set<Pair<Int, Int>>, List<Pair<String, Int>>> {
        val parts = input.split("\n\n")
        val startingSheet = parts[0].split("\n")
            .map { l ->
                val nums = l.split(",").map { it.toInt() }
                Pair(nums[0], nums[1])
            }
            .toSet()
        val instructions = parts[1].split("\n").map { l ->
            val foldLine = l.split(" ")[2].split("=")
            Pair(foldLine[0], foldLine[1].toInt())
        }
        return Pair(startingSheet, instructions)
    }

    fun solvePartOne(input: String): Int {
        val (startingSheet, instructions) = parseInput(input)

        val afterFolding = instructions.take(1).fold(startingSheet) { acc, instruction ->
            when (instruction.first) {
                "x" -> foldAlongVerticalLine(acc, instruction.second)
                "y" -> foldAlongHorizontalLine(acc, instruction.second)
                else -> TODO()
            }
        }
        return afterFolding.size
    }

    fun solvePartTwo(input: String): Int {
        val (startingSheet, instructions) = parseInput(input)
        val afterFolding = instructions.fold(startingSheet) { acc, instruction ->
            when (instruction.first) {
                "x" -> foldAlongVerticalLine(acc, instruction.second)
                "y" -> foldAlongHorizontalLine(acc, instruction.second)
                else -> TODO()
            }
        }
        printSheet(afterFolding)
        return afterFolding.size
    }

}