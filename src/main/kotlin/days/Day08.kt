package days

private fun decode(line: String): Int {
//     A
//    F B
//     G
//    E C
//     D
//
//    1 ->  BC     : 2
//    4 ->  BC  FG : 4
//    7 -> ABC     : 3
//    8 -> ABCDEFG : 7
//
//    2 -> AB DE G : 5
//    3 -> ABCD  G : 5
//    5 -> A CD FG : 5
//
//    0 -> ABCDEF  : 6
//    6 -> A CDEFG : 6
//    9 -> ABCD FG : 6
//

//    FG  -> in 4 but not in 1
//    BC  -> contents of 1
//    ABC -> contents of 7
//
//    0 -> length 6, doesn't have FG
//    6 -> length 6, contains all of FG but doesn't contain all of BC
//    9 -> length 6, contains all of FG and all of BC
//
//    5 -> length 5, contains all of FG
//    3 -> length 5, contains all of ABC
//    2 -> length 5, contains neither of FG nor ABC


    val (inputSignals, outputToDecode) = line
        .split(" | ")
        .map { it.split(" ").map { inner -> inner.toSet() } }

    val one = inputSignals.first { it.size == 2 }
    val four = inputSignals.first { it.size == 4 }
    val seven = inputSignals.first { it.size == 3 }

    val bc = one // segments bc are used in 1
    val fg = four - one // this is set difference, giving us only the segments present in 4, but not in 1
    val abc = seven // segments abc are used in 7

    val wiresToValue = mapOf(
        inputSignals.first { it.size == 6 && !it.containsAll(fg) } to 0,
        one to 1,
        inputSignals.first { it.size == 5 && !it.containsAll(fg) && !it.containsAll(abc) } to 2,
        inputSignals.first { it.size == 5 && it.containsAll(abc) } to 3,
        four to 4,
        inputSignals.first { it.size == 5 && it.containsAll(fg) } to 5,
        inputSignals.first { it.size == 6 && it.containsAll(fg) && !it.containsAll(bc) } to 6,
        seven to 7,
        inputSignals.first { it.size == 7 } to 8,
        inputSignals.first { it.size == 6 && it.containsAll(fg) && it.containsAll(bc) } to 9
    )

    return outputToDecode.fold(0) { acc, nextDigit -> acc * 10 + wiresToValue[nextDigit]!! }
}

class Day08 : Day(8) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(inputList: List<String>): Int {
        return inputList.flatMap {
            it.split(" | ")[1].split(" ").map { s -> s.length }
        }.count { it == 2 || it == 3 || it == 4 || it == 7 }
    }

    fun solvePartTwo(inputList: List<String>): Int {
        return inputList.sumOf { decode(it) };
    }
}