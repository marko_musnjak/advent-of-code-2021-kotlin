package days

class Day06 : Day(6) {
    override fun partOne(): Long {
        return solvePartOne(inputString)
    }

    override fun partTwo(): Long {
        return solvePartTwo(inputString)
    }

    private fun createNextFishGeneration(stateAtStart: Map<Int, Long>, day: Int): Map<Int, Long> =
        mapOf(
            0 to stateAtStart.getOrDefault(1, 0),
            1 to stateAtStart.getOrDefault(2, 0),
            2 to stateAtStart.getOrDefault(3, 0),
            3 to stateAtStart.getOrDefault(4, 0),
            4 to stateAtStart.getOrDefault(5, 0),
            5 to stateAtStart.getOrDefault(6, 0),
            6 to stateAtStart.getOrDefault(7, 0) + stateAtStart.getOrDefault(0, 0),
            7 to stateAtStart.getOrDefault(8, 0),
            8 to stateAtStart.getOrDefault(0, 0)
        )

    fun solvePartOne(inputString: String): Long {
        val fishAtStart = inputString
            .split(",")
            .map { it.toInt() }
            .groupingBy { it }
            .eachCount()
            .mapValues { it.value.toLong() }
        // key is time, value is count
        val finalState = (1..80)
            .fold(fishAtStart) { stateAtStart, day -> createNextFishGeneration(stateAtStart, day) }
        return finalState.values.sum()
    }

    fun solvePartTwo(inputString: String): Long {
        val fishAtStart = inputString
            .split(",")
            .map { it.toInt() }
            .groupingBy { it }
            .eachCount()
            .mapValues { it.value.toLong() }
        // key is time, value is count
        val finalState = (1..256)
            .fold(fishAtStart) { stateAtStart, day -> createNextFishGeneration(stateAtStart, day) }
        return finalState.values.sum()
    }
}