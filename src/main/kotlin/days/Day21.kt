package days

class Day21 : Day(21) {
    override fun partOne(): Long {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Long {
        return solvePartTwo(inputList)
    }

    private fun deterministicDice(): Sequence<Int> {
        val startingValue = 1
        val increment = 1
        val sides = 100
        var currentPosition = startingValue - increment
        return sequence {
            while (true) {
                currentPosition += increment
                if (currentPosition > sides) currentPosition -= sides
                yield(currentPosition)
            }
        }
    }

    fun solvePartOne(input: List<String>): Long {
        val inputPositions = input.map { it.split(" ") }.associate { Pair(it[1].toInt(), it[4].toInt()) }
        val positions = Array(inputPositions.size) { inputPositions.getOrDefault(it + 1, 0) }
        val playerScores = Array(inputPositions.size) { 0L }
        val diceRolls = deterministicDice()

        val positionValues = (0..9).map { if (it == 0) 10 else it }

        val numPlayers = inputPositions.size

        var currentPlayer = 0
        var diceRolled = 0
        while (playerScores.none { it >= 1000 }) {
            val rolled = diceRolls.take(3).toList()
            diceRolled += 3
            positions[currentPlayer] = (positions[currentPlayer] + rolled.sum()) % 10
            playerScores[currentPlayer] = playerScores[currentPlayer] + positionValues[positions[currentPlayer]]
            currentPlayer = (currentPlayer + 1) % numPlayers
        }

        return diceRolled * (playerScores.minOrNull() ?: 0)
    }

    private data class PlayerState(val position: Int, val score: Int)

    private data class Accumulator(
        val round: Int,
        val wins: Map<Int, Long>,
        val multiverseState: Map<Map<Int, PlayerState>, Long>
    )

    private fun playAllTheGames(
        round: Int,
        wins: Map<Int, Long>,
        multiverseState: Map<Map<Int, PlayerState>, Long>
    ): Long {
        if (multiverseState.isEmpty()) return wins.values.maxOrNull() ?: 0
        val playerIndex = 2 - round % 2

        // we roll once , our distribution of sums is
        val diceDistribution = mapOf(
            3 to 1L,
            4 to 3L,
            5 to 6L,
            6 to 7L,
            7 to 6L,
            8 to 3L,
            9 to 1L,
        )

        val statesAfterTheRound = multiverseState
            .flatMap { universeEntry ->
                val universe = universeEntry.key
                val universeCount = universeEntry.value
                val currentPosition = universe[playerIndex]!!.position
                val currentScore = universe[playerIndex]!!.score
                diceDistribution.map { distribution ->
                    val newPosition =
                        if (currentPosition + distribution.key > 10) currentPosition + distribution.key - 10 else currentPosition + distribution.key
                    val newScore = currentScore + newPosition
                    val newPlayerState = PlayerState(newPosition, newScore)
                    val newUniverse = universe + (playerIndex to newPlayerState)
                    val newCount = universeCount * distribution.value
                    newUniverse to newCount
                }
            }
            .groupBy { it.first }
            .mapValues { it.value.sumOf { r -> r.second } }

        // now we filter out those where there were wins
        val unresolvedUniverses = statesAfterTheRound.filter { it.key.none { v -> v.value.score >= 21 } }
        val winsInThisRound =
            statesAfterTheRound.filter { it.key.any { v -> v.value.score >= 21 } }.mapValues { it.value }.values.sum()
        val newWinState = wins + (playerIndex to ((wins[playerIndex] ?: 0L) + winsInThisRound))
        return playAllTheGames(round + 1, newWinState, unresolvedUniverses)
    }

    fun solvePartTwo(input: List<String>): Long {
        val inputPositions =
            input.map { it.split(" ") }.associate { Pair(it[1].toInt(), PlayerState(it[4].toInt(), 0)) }

        val multiverseState = mapOf(inputPositions to 1L)

        return playAllTheGames(1, mapOf(1 to 0L, 2 to 0L), multiverseState)
    }
}

