package days

class Day01 : Day(1) {

    override fun partOne(): Int {
        return solvePartOne(inputListOfInts)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputListOfInts)
    }

    fun solvePartOne(nums: List<Int>): Int {
        return nums
            .windowed(2)
            .count { it[0] < it[1] }
    }

    fun solvePartTwo(nums: List<Int>): Int {
        return nums
            .windowed(3)
            .map { it.sum() }
            .windowed(2)
            .count { it[0] < it[1] }
    }

}
