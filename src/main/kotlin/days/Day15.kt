package days

import java.util.*
import kotlin.math.abs

data class DijkstraContainer(val distance: Int, val previous: Point)

class Day15 : Day(15) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    private fun getHorizontalNeighbors(point: Point, boundingBox: Point): List<Point> =
        listOfNotNull(
            if (point.x > 0) Point(point.x - 1, point.y) else null,
            if (point.y > 0) Point(point.x, point.y - 1) else null,
            if (point.y < boundingBox.y - 1) Point(point.x, point.y + 1) else null,
            if (point.x < boundingBox.x - 1) Point(point.x + 1, point.y) else null,
        )

    fun solvePartOne(input: List<String>): Int {
        val startingPosition = input.map { l -> l.map { it.code - '0'.code } }

        val lowerRightCorner = Point(startingPosition.size, startingPosition.maxOf { it.size })

        // : Map<Point,Map<Point, Int>>
        val graph = startingPosition
            .flatMapIndexed { rowIndex, row ->
                List(row.size) { col ->
                    val currentPoint = Point(rowIndex, col)
                    getHorizontalNeighbors(currentPoint, lowerRightCorner)
                        .map { currentPoint to (it to startingPosition[it.x][it.y]) }
                        .groupBy { it.first }
                        .mapValues { it.value.associate { item -> item.second } }
                }
            }
            .fold(emptyMap<Point, Map<Point, Int>>()) { acc, map ->
                acc.plus(map)
            }

        var dijkstraState = mutableMapOf<Point, DijkstraContainer>()
        val queue =
            PriorityQueue<Point> { a, b ->
                (dijkstraState[a!!]?.distance ?: Int.MAX_VALUE).compareTo(dijkstraState[b!!]?.distance ?: Int.MAX_VALUE)
            }

        graph.keys.filter { it != Point(0, 0) }.forEach {
            queue.add(it)
        }

        dijkstraState[Point(0, 0)] = DijkstraContainer(0, Point(0, 0))
        queue.add(Point(0, 0))

        while (!queue.isEmpty()) {
            val minPoint = queue.remove()
            val neighbors = getHorizontalNeighbors(minPoint, lowerRightCorner)

            // for each neighbor still in queue
            neighbors
                .filter { queue.contains(it) }
                .forEach {
                    val alternativeDistance = dijkstraState[minPoint]!!.distance + graph[minPoint]!![it]!!
                    if (alternativeDistance < (dijkstraState[it]?.distance ?: Int.MAX_VALUE)) {
                        queue.remove(it)
                        dijkstraState[it] = DijkstraContainer(alternativeDistance, minPoint)
                        queue.add(it)
                    }
                }
        }
        return dijkstraState[Point(startingPosition.size - 1, startingPosition.maxOf { it.size } - 1)]!!.distance
    }

    private fun inflate(riskLevels: List<List<Int>>, n: Int): List<List<Int>> {

        val transform = { i: Int, j: Int ->
            val ret = (i + j) % 9
            if (ret != 0) ret else 9
        }

        val nRows = riskLevels.size
        val nCols = riskLevels[0].size

        val ret = Array(nRows * n) { Array(nCols * n) { 0 } }

        for (row in 0 until nRows) {
            for (col in 0 until nCols) {
                for (i in 0 until n) {
                    for (j in 0 until n) {
                        ret[row + i * nRows][col + j * nCols] =
                            transform(riskLevels[row][col], i + j)
                    }
                }
            }
        }

        return ret.map { it.toList() }
    }

    private fun manhattanDistance(a: Point, b: Point): Int = abs(a.x - b.x) + abs(a.y - b.y)

    fun solvePartTwo(input: List<String>): Int {
        // TODO: Make this task run at reasonable speed

        val template = input.map { l -> l.map { it.code - '0'.code } }

        val startingPosition = inflate(template, 5)

        val lowerRightCorner = Point(startingPosition.size, startingPosition.maxOf { it.size })

        // : Map<Point,Map<Point, Int>>
        val graph = startingPosition
            .flatMapIndexed { rowIndex, row ->
                List(row.size) { col ->
                    val currentPoint = Point(rowIndex, col)
                    getHorizontalNeighbors(currentPoint, lowerRightCorner)
                        .map { currentPoint to (it to startingPosition[it.x][it.y]) }
                        .groupBy { it.first }
                        .mapValues { it.value.associate { item -> item.second } }
                }
            }
            .fold(emptyMap<Point, Map<Point, Int>>()) { acc, map ->
                acc.plus(map)
            }

        val dijkstraState = mutableMapOf<Point, DijkstraContainer>()
        val queue =
            PriorityQueue<Point> { a, b ->
                (dijkstraState[a!!]?.distance?.plus(manhattanDistance(a!!, lowerRightCorner)) ?: Int.MAX_VALUE).compareTo(
                    dijkstraState[b!!]?.distance?.plus(manhattanDistance(a!!, lowerRightCorner)) ?: Int.MAX_VALUE
                )
            }

        graph.keys.filter { it != Point(0, 0) }.forEach {
            queue.add(it)
        }

        dijkstraState[Point(0, 0)] = DijkstraContainer(0, Point(0, 0))
        queue.add(Point(0, 0))

        while (!queue.isEmpty()) {
            val minPoint = queue.remove()
            val neighbors = getHorizontalNeighbors(minPoint, lowerRightCorner)

            // for each neighbor still in queue
            neighbors
                .filter { queue.contains(it) }
                .forEach {
                    val alternativeDistance = dijkstraState[minPoint]!!.distance + graph[minPoint]!![it]!!
                    if (alternativeDistance < (dijkstraState[it]?.distance ?: Int.MAX_VALUE)) {
                        queue.remove(it)
                        dijkstraState[it] = DijkstraContainer(alternativeDistance, minPoint)
                        queue.add(it)
                    }
                }
        }
        return dijkstraState[Point(startingPosition.size - 1, startingPosition.maxOf { it.size } - 1)]!!.distance
    }

}