package days

class Day18 : Day(18) {

    private sealed class SnailNumber {
        abstract fun magnitude(): Int
        abstract fun addToLeftMost(value: Int)
        abstract fun addToRightMost(value: Int)
        abstract fun split(): Boolean
        abstract fun explode(depth: Int = 0): Pair<Int, Int>?

        fun reduce() {
            do {
                val exploded = explode() != null
                val split = if (!exploded) split() else false
            } while (exploded || split)
        }

        companion object {
            fun from(line: String): SnailNumber {
                return when (line.length == 1) {
                    true -> SnailLiteral.from(line)
                    false -> SnailPair.from(line)
                }
            }

            fun from(left: SnailNumber, right: SnailNumber): SnailNumber {
                return SnailPair(left, right)
            }
        }
    }

    private class SnailLiteral(var value: Int) : SnailNumber() {
        override fun magnitude() = value
        override fun addToLeftMost(value: Int) = run { this.value += value }
        override fun addToRightMost(value: Int) = run { this.value += value }
        override fun split() = false
        override fun explode(depth: Int): Pair<Int, Int>? = null
        override fun toString() = "$value"

        companion object {
            fun from(line: String) = SnailLiteral(line.toInt())
        }
    }

    private class SnailPair(var left: SnailNumber, var right: SnailNumber) : SnailNumber() {
        override fun magnitude() = 3 * left.magnitude() + 2 * right.magnitude()
        override fun addToLeftMost(value: Int) = left.addToLeftMost(value)
        override fun addToRightMost(value: Int) = right.addToRightMost(value)
        override fun toString() = "[$left,$right]"

        override fun split(): Boolean {
            if (left is SnailLiteral) {
                (left as SnailLiteral).value.let {
                    if (it >= 10) {
                        left = SnailPair(SnailLiteral(it / 2), SnailLiteral((it + 1) / 2))
                        return true
                    }
                }
            }
            if (left.split()) {
                return true
            }
            if (right is SnailLiteral) {
                (right as SnailLiteral).value.let {
                    if (it >= 10) {
                        right = SnailPair(SnailLiteral(it / 2), SnailLiteral((it + 1) / 2))
                        return true
                    }
                }
            }
            return right.split()
        }

        override fun explode(depth: Int): Pair<Int, Int>? {
            if (depth == 4) {
                return (left as SnailLiteral).value to (right as SnailLiteral).value
            }
            left.explode(depth + 1)?.let { (first, second) ->
                if (first != -1 && second != -1) {
                    this.left = SnailLiteral(0)
                    this.right.addToLeftMost(second)
                    return first to -1
                }
                if (second != -1) {
                    this.right.addToLeftMost(second)
                    return -1 to -1
                }
                return first to -1
            }
            right.explode(depth + 1)?.let { (first, second) ->
                if (first != -1 && second != -1) {
                    this.right = SnailLiteral(0)
                    this.left.addToRightMost(first)
                    return -1 to second
                }
                if (first != -1) {
                    this.left.addToRightMost(first)
                    return -1 to -1
                }
                return -1 to second
            }
            return null
        }

        companion object {
            fun from(line: String): SnailNumber {
                val inside = line.drop(1).dropLast(1)
                var left = ""
                var brackets = 0
                for (c in inside) {
                    if (c == '[') brackets++
                    if (c == ']') brackets--
                    if (c == ',' && brackets == 0) break
                    left += c
                }
                val right = inside.drop(left.length + 1)
                return SnailPair(SnailNumber.from(left), SnailNumber.from(right))
            }
        }
    }

    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(input: List<String>): Int {
        return input
            .map { SnailNumber.from(it) }
            .reduce { acc, s ->
                SnailNumber.from(acc, s).apply { reduce() }
            }
            .magnitude()
    }

    fun solvePartTwo(input: List<String>): Int {
        val snailNumbers = input.map { SnailNumber.from(it) } // Need to implement equality to make this worthwhile
        return input.maxOf { first ->
            input
                .filterNot { it == first }
                .maxOf { second ->
                    SnailNumber.from("[$first,$second]").apply { reduce() }.magnitude()
                }
        }
    }

}