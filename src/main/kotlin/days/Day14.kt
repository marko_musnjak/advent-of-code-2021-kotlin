package days

class Day14 : Day(14) {
    override fun partOne(): Long {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Long {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(input: List<String>): Long {
        val initialString = input[0]
        val mappings = input
            .drop(2)
            .associate {
                val split = it.split(" -> ")
                split[0] to split[1]
            }

        val initialPairs = initialString.windowed(2, 1).groupingBy { it }.eachCount().mapValues { it.value.toLong() }
        val initialCounts = initialString.groupingBy { it }.eachCount().mapValues { it.value.toLong() }

        val elementCounts = (0 until 10)
            .fold(Pair(initialPairs, initialCounts)) { acc, _ ->
                singleFoldStep(acc, mappings)
            }
            .second

        return elementCounts.values.maxOrNull()!! - elementCounts.values.minOrNull()!!
    }

    private fun singleFoldStep(
        currentState: Pair<Map<String, Long>, Map<Char, Long>>,
        mappings: Map<String, String>
    ): Pair<Map<String, Long>, Map<Char, Long>> {
        val aggregate = currentState.first.asIterable()
            .fold(Pair(emptyList<Pair<String, Long>>(), currentState.second)) { collected, item ->
                val currentPairs = collected.first

                val newChar = mappings[item.key]!!

                val newElementCountMap = collected.second.toMutableMap()
                newElementCountMap.merge(newChar[0], item.value) { a, b -> a + b }

                val newPairs = listOf(
                    Pair(item.key[0] + newChar, item.value),
                    Pair(newChar + item.key[1], item.value)
                )

                Pair(
                    currentPairs + newPairs,
                    newElementCountMap
                )
            }
        val updatedPairCounts = aggregate.first
            .groupBy { it.first }
            .mapValues { item -> item.value.sumOf { it.second } }

        val updatedElementCounts = aggregate.second

        return Pair(updatedPairCounts, updatedElementCounts)
    }

    fun solvePartTwo(input: List<String>): Long {
        val initialString = input[0]
        val mappings = input
            .drop(2)
            .associate {
                val split = it.split(" -> ")
                split[0] to split[1]
            }

        val initialPairs = initialString.windowed(2, 1).groupingBy { it }.eachCount().mapValues { it.value.toLong() }
        val initialCounts = initialString.groupingBy { it }.eachCount().mapValues { it.value.toLong() }

        val elementCounts = (0 until 40)
            .fold(Pair(initialPairs, initialCounts)) { acc, _ ->
                singleFoldStep(acc, mappings)
            }
            .second

        return elementCounts.values.maxOrNull()!! - elementCounts.values.minOrNull()!!
    }

}