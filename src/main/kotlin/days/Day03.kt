package days

fun Boolean.toInt() = if (this) 1 else 0

class Day03 : Day(3) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(input: List<String>): Int {
        val inputNumbers = input.map { Integer.parseInt(it, 2) }
        val inputLengths = input.maxOf { it.length }
        val count = inputNumbers.size
        val bits = (inputLengths - 1 downTo 0).map { bitPosition ->
            Pair(
                inputNumbers.map { it and 1.shl(bitPosition) }.count { it > 0 } > count / 2,
                inputNumbers.map { it and 1.shl(bitPosition) }.count { it > 0 } < count / 2
            )
        }
        val outputValues = bits.fold(Pair(0, 0)) { acc, bitPair ->
            Pair(
                acc.first * 2 + bitPair.first.toInt(),
                acc.second * 2 + bitPair.second.toInt()
            )
        }
        return outputValues.first * outputValues.second
    }

    private fun findMajorityBit(items: List<Int>, position: Int): Int {
        return items
            .map { it and 1.shl(position) }
            .groupingBy { it > 0 }
            .eachCount()
            .let {
                if (it[true] == it[false])
                    true
                else
                    it.maxByOrNull { g -> g.value }!!.key
            }
            .toInt()
    }

    fun solvePartTwo(input: List<String>): Int {
        val inputNumbers = input.map { Integer.parseInt(it, 2) }
        val inputLengths = input.maxOf { it.length }

        val finalNumbers = (inputLengths - 1 downTo 0)
            .fold(Pair(inputNumbers, inputNumbers)) { acc, bitPosition ->
                val majorityBitForFirst = findMajorityBit(acc.first, bitPosition)
                val majorityBitForSecond = findMajorityBit(acc.second, bitPosition)

                Pair(
                    if (acc.first.size == 1) acc.first else acc.first.filter {
                        it and 1.shl(bitPosition) == majorityBitForFirst.shl(
                            bitPosition
                        )
                    }, // has majority bit set at position
                    if (acc.second.size == 1) acc.second else acc.second.filter {
                        it and 1.shl(bitPosition) != majorityBitForSecond.shl(bitPosition)
                    } // has minority bit set at position
                )

            }

        return finalNumbers.first.first() * finalNumbers.second.first()
    }

}