package days

import kotlin.math.min

class Day11 : Day(11) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    private fun getNeighbors(point: Point, lowerRight: Point): List<Point> =
        listOfNotNull(
            if (point.x > 0 && point.y > 0) Point(point.x - 1, point.y - 1) else null,
            if (point.x > 0) Point(point.x - 1, point.y) else null,
            if (point.x > 0 && point.y < lowerRight.y) Point(point.x - 1, point.y + 1) else null,

            if (point.y > 0) Point(point.x, point.y - 1) else null,
            if (point.y < lowerRight.y) Point(point.x, point.y + 1) else null,

            if (point.x < lowerRight.x && point.y > 0) Point(point.x + 1, point.y - 1) else null,
            if (point.x < lowerRight.x) Point(point.x + 1, point.y) else null,
            if (point.x < lowerRight.x && point.y < lowerRight.y) Point(point.x + 1, point.y + 1) else null,
        )

    private tailrec fun expandWave(
        startingState: List<List<Int>>,
        updatedPreviously: List<Point>,
        boundingBox: Point
    ): List<List<Int>> {
        return if (updatedPreviously.isEmpty()) {
            startingState.map { line ->
                line.map { if (it == 10) 0 else it }
            }
        } else {
            val allTheNeighborsThatHaveNotFlashed = updatedPreviously
                .flatMap { getNeighbors(it, boundingBox) }
                .filter { startingState[it.x][it.y] != 10 }

            val energyGrowth = allTheNeighborsThatHaveNotFlashed.groupingBy { it }.eachCount()

            val nextFront = startingState.mapIndexed { rowIndex, row ->
                row.mapIndexed { colIndex, num ->
                    min(num + energyGrowth.getOrDefault(Point(rowIndex, colIndex), 0), 10)
                }
            }

            // keys of energyGrowth that are now 10
            val pointsThatJustFlashed = energyGrowth.keys.filter { nextFront[it.x][it.y] == 10 }
            return expandWave(nextFront, pointsThatJustFlashed, boundingBox)
        }
    }

    private fun oneRound(startingState: List<List<Int>>): List<List<Int>> {
        val boundingBox = Point(startingState.size - 1, startingState[0].size - 1)
        val afterIncrease = startingState.map { row -> row.map { it + 1 }.toMutableList() }
        // find the points that will flash
        val startingPointsForWave = afterIncrease
            .flatMapIndexed { row, line ->
                line.mapIndexedNotNull { col, num -> if (num == 10) Point(row, col) else null }
            }

        return expandWave(afterIncrease, startingPointsForWave, boundingBox)
    }

    fun solvePartOne(input: List<String>): Int {
        val startingPosition = input.map { l -> l.map { it.code - '0'.code } }
        val result = (1..100).fold(Pair(0, startingPosition)) { acc, _ ->
            val nextState = oneRound(acc.second)
            val flashedCount = nextState.flatten().count { it == 0 }
            Pair(acc.first + flashedCount, nextState)
        }

        return result.first
    }

    private tailrec fun findCompleteFlash(l: List<List<Int>>, currentRound: Int): Int {
        val nextState = oneRound(l)
        val allZeros = nextState.flatten().all { it == 0 }
        return if (allZeros) currentRound else findCompleteFlash(nextState, currentRound + 1)
    }

    fun solvePartTwo(input: List<String>): Int {
        val startingPosition = input.map { l -> l.map { it.code - '0'.code } }
        return findCompleteFlash(startingPosition, 1)
    }
}
