package days

import java.util.*
import kotlin.math.abs

data class Point(val x: Int, val y: Int) {
    constructor(str: String) : this(str.split(",")[0].toInt(), str.split(",")[1].toInt())
}

data class Line(val start: Point, val end: Point) {
    constructor(str: String) : this(Point(str.split(" -> ")[0]), Point(str.split(" -> ")[1]))

    fun isVertical(): Boolean {
        return start.x == end.x
    }

    fun isHorizontal(): Boolean {
        return start.y == end.y
    }

    fun isDiagonal(): Boolean {
        return abs(start.x - end.x) == abs(start.y - end.y)
    }

    fun draw(): List<Point> {
        val points = LinkedList<Point>()

        val distanceX = abs(end.x - start.x)
        val stepX = if (start.x < end.x) 1 else -1
        val distanceY = -abs(end.y - start.y)
        val stepY = if (start.y < end.y) 1 else -1
        var err = distanceX + distanceY
        var currentX = start.x
        var currentY = start.y
        while (true) {
            points.add(Point(currentX, currentY))
            if (currentX == end.x && currentY == end.y) break
            val e2 = 2 * err
            if (e2 >= distanceY) {
                err += distanceY
                currentX += stepX
            }
            if (e2 <= distanceX) {
                err += distanceX
                currentY += stepY
            }
        }

        return points
    }
}

class Day05 : Day(5) {
    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(inputList: List<String>): Int {
        return inputList
            .map { Line(it) }
            .filter { it.isVertical() || it.isHorizontal() }
            .flatMap { it.draw() }
            .groupingBy { it }
            .eachCount()
            .count { it.value > 1 }
    }

    fun solvePartTwo(inputList: List<String>): Int {
        return inputList
            .map { Line(it) }
            .filter { it.isVertical() || it.isHorizontal() || it.isDiagonal() }
            .flatMap { it.draw() }
            .groupingBy { it }
            .eachCount()
            .count { it.value > 1 }
    }
}