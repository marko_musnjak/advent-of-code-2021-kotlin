package days

typealias  Matrix = List<List<Int>>

fun Matrix.determinant(): Int {
    return 0 +
            this[0][0] * (this[1][1] * this[2][2] - this[1][2] * this[2][1]) -
            this[0][1] * (this[1][0] * this[2][2] - this[1][2] * this[2][0]) +
            this[0][2] * (this[1][0] * this[2][1] - this[1][1] * this[2][0])
}

private fun Matrix.multiplyBy(v: Vector) = Vector(
    this[0][0] * v.x + this[0][1] * v.y + this[0][2] * v.z,
    this[1][0] * v.x + this[1][1] * v.y + this[1][2] * v.z,
    this[2][0] * v.x + this[2][1] * v.y + this[2][2] * v.z
)

private data class Vector(val x: Int, val y: Int, val z: Int) {

    companion object {
        fun from(string: String): Vector {
            val split = string.split(",").map { it.toInt() }
            return Vector(split[0], split[1], split[2])
        }
    }

    operator fun minus(other: Vector) = Vector(this.x - other.x, this.y - other.y, this.z - other.z)
}

class Day19 : Day(19) {

    private val rotations =
        listOf(-1, 1).flatMap { x ->
            listOf(-1, 1).flatMap { y ->
                listOf(-1, 1).flatMap { z ->
                    // six permutations of the set {1, 2, 3}, namely (1, 2, 3), (1, 3, 2), (2, 1, 3), (2, 3, 1), (3, 1, 2), and (3, 2, 1)
                    // [x,0,0], [0,y,0], [0,0,z]
                    listOf(
                        // 1,2,3
                        listOf(
                            listOf(x, 0, 0),
                            listOf(0, y, 0),
                            listOf(0, 0, z),
                        ) as Matrix,
                        // 1,3,2
                        listOf(
                            listOf(x, 0, 0),
                            listOf(0, 0, z),
                            listOf(0, y, 0),
                        ) as Matrix,
                        // 2,1,3
                        listOf(
                            listOf(0, y, 0),
                            listOf(x, 0, 0),
                            listOf(0, 0, z),
                        ) as Matrix,
                        // 2,3,1
                        listOf(
                            listOf(0, y, 0),
                            listOf(0, 0, z),
                            listOf(x, 0, 0),
                        ) as Matrix,
                        // 3,1,2
                        listOf(
                            listOf(0, 0, z),
                            listOf(x, 0, 0),
                            listOf(0, y, 0),
                        ) as Matrix,
                        // 3,2,1
                        listOf(
                            listOf(0, 0, z),
                            listOf(0, y, 0),
                            listOf(x, 0, 0),
                        ) as Matrix
                    )
                }
            }
        }.filter { it.determinant() == 1 } // This ensures we only keep the right-handed coordinate systems

    override fun partOne(): Int {
        return solvePartOne(inputString)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputString)
    }

    fun solvePartOne(input: String): Int {
        val scannerGroups = input.split("\n\n")
            .map { it.split("\n").drop(1).filter { l -> l.isNotEmpty() }.map { line -> Vector.from(line) } }

        val allUnrotatedInternalGroupVectors = scannerGroups.map { scannerGroup ->
            scannerGroup.flatMap { first ->
                scannerGroup.flatMap { second ->
                    if (first == second) emptyList() else listOf(
                        first - second,
                        second - first
                    )
                }
            }.toSet()
        }

        val allRotationsOfAllGroupVectors = allUnrotatedInternalGroupVectors.map { vectors ->
            rotations.map { rotation ->
                vectors.map { v -> rotation.multiplyBy(v) }.toSet()
            }
        }

        val referenceGroup = allUnrotatedInternalGroupVectors[0]

//        val allIntersections = allRotationsOfAllGroupVectors.map { rot ->
//            rot.intersect(group0.toSet()).size
//        }

        val intersectionSizeByRotation = allRotationsOfAllGroupVectors.drop(1).map { group ->
            group.map { rotatedGroup ->
                rotatedGroup.intersect(referenceGroup).size
            }
        }


        return 0
    }

    fun solvePartTwo(input: String): Int {
        return 0
    }

}