package days

import java.util.*

private const val START_NODE = 0
private const val UPPERCASE_OFFSET = 100_000
private const val END_NODE = 1_000_000

class Day12 : Day(12) {

    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    private tailrec fun continuePathsForPartOne(
        currentPaths: List<List<String>>,
        goal: String,
        graph: Map<String, List<String>>
    ): List<List<String>> {
        val expanded = currentPaths.flatMap { path ->
            if (path.last() == goal) listOf(path) else {
                // find next steps from path.last
                val validNextSteps =
                    graph
                        .getOrDefault(path.last(), emptyList())
                        // filter out elements that are already in path, except the uppercase ones
                        .filter { !path.contains(it) || it.uppercase() == it }
                validNextSteps.map { path + it }
            }
        }
        return if (expanded.all { it.last() == goal }) expanded else continuePathsForPartOne(expanded, goal, graph)
    }

    fun solvePartOne(input: List<String>): Int {
        val graph = input
            .flatMap {
                val split = it.split("-")
                listOf(split[0] to split[1], split[1] to split[0])
            }
            .filter { it.first != "end" && it.second != "start" }
            .groupBy({ it.first }, { it.second })

        return continuePathsForPartOne(listOf(listOf("start")), "end", graph).size
    }

    private tailrec fun continuePathsForPartTwo(
        currentPaths: List<List<Int>>,
        completedPaths: List<List<Int>>,
        goal: Int,
        graph: Map<Int, List<Int>>
    ): List<List<Int>> {
        // There are probably ways to improve performance here, but just avoiding handling strings was a big boost
        val expanded = currentPaths.flatMap { path ->

            // if the next step is lowercase, and present in path twice, reject it
            val possibleNextSteps = graph
                .getOrDefault(path.first(), emptyList())
                .filter { nextStep ->
                    path
                        .filter { it < UPPERCASE_OFFSET }
                        .count { it != START_NODE && it == nextStep } < 2
                }
            val expandedPaths = possibleNextSteps.map {
                val newList = LinkedList(path)
                newList.addFirst(it)
                newList
            }

            expandedPaths.filter { newPath ->
                newPath
                    .filter { it < UPPERCASE_OFFSET }
                    .groupingBy { it }.eachCount()
                    .values
                    .count { it > 1 } <= 1
            }
        }
        val groupedPaths = expanded.groupBy { it.first() == goal }
        val newlyCompleted = groupedPaths.getOrDefault(true, emptyList())
        val pathsStillInProgress = groupedPaths.getOrDefault(false, emptyList())
        return if (pathsStillInProgress.isEmpty()) completedPaths + expanded else continuePathsForPartTwo(
            expanded,
            completedPaths + newlyCompleted,
            goal,
            graph
        )
    }

    fun solvePartTwo(input: List<String>): Int {
        val preParsed = input.map {
            val split = it.split("-")
            listOf(split[0], split[1])
        }

        val intNodes = preParsed
            .flatten()
            .distinct()
            .mapIndexed { index, node ->
                when (node) {
                    "start" -> Pair(node, START_NODE)
                    "end" -> Pair(node, END_NODE)
                    else -> Pair(node, 1 + if (node[0].isUpperCase()) index + UPPERCASE_OFFSET else index)
                }
            }
            .toMap()

        val graph = preParsed
            .flatMap { listOf(intNodes[it[0]]!! to intNodes[it[1]]!!, intNodes[it[1]]!! to intNodes[it[0]]!!) }
            .filter { it.first != END_NODE && it.second != START_NODE }
            .groupBy({ it.first }, { it.second })

        val startingList = LinkedList<Int>()
        startingList.add(START_NODE)
        val paths = continuePathsForPartTwo(listOf(startingList), emptyList(), END_NODE, graph)
        return paths.size
    }

}