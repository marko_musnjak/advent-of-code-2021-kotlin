package days

import kotlin.collections.ArrayDeque

class Day09 : Day(9) {
    private fun getNeighbors(point: Point, boundingBox: Point): List<Point> =
        listOfNotNull(
            if (point.x > 0 && point.y > 0) Point(point.x - 1, point.y - 1) else null,
            if (point.x > 0) Point(point.x - 1, point.y) else null,
            if (point.x > 0 && point.y < boundingBox.y - 1) Point(point.x - 1, point.y + 1) else null,

            if (point.y > 0) Point(point.x, point.y - 1) else null,
            if (point.y < boundingBox.y - 1) Point(point.x, point.y + 1) else null,

            if (point.x < boundingBox.x - 1 && point.y > 0) Point(point.x + 1, point.y - 1) else null,
            if (point.x < boundingBox.x - 1) Point(point.x + 1, point.y) else null,
            if (point.x < boundingBox.x - 1 && point.y < boundingBox.y - 1) Point(point.x + 1, point.y + 1) else null,
        )

    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(input: List<String>): Int {
        val heightMap = input.map { it.toList().map { c -> c.code - '0'.code } }
        val boundingBox = Point(heightMap.size, heightMap.maxOfOrNull { it.size } ?: 0)
        return heightMap
            .flatMapIndexed { rowIndex, row ->
                row.flatMapIndexed { colIndex, height ->
                    val allNeighborsHigher = getNeighbors(Point(rowIndex, colIndex), boundingBox)
                        .map { heightMap[it.x][it.y] }
                        .all { it > height }
                    if (allNeighborsHigher) listOf(heightMap[rowIndex][colIndex] + 1) else emptyList()
                }
            }
            .sum()
    }

    private fun getHorizontalNeighbors(point: Point, boundingBox: Point): List<Point> =
        listOfNotNull(
            if (point.x > 0) Point(point.x - 1, point.y) else null,
            if (point.y > 0) Point(point.x, point.y - 1) else null,
            if (point.y < boundingBox.y - 1) Point(point.x, point.y + 1) else null,
            if (point.x < boundingBox.x - 1) Point(point.x + 1, point.y) else null,
        )

    private tailrec fun depthFirstSearch(
        point: Point,
        queue: ArrayDeque<Point>,
        foundPoints: MutableSet<Point>,
        boundingBox: Point,
        heightMap: List<List<Int>>
    ): Set<Point> {
        val neighbors = getHorizontalNeighbors(point, boundingBox)
            .filter { !foundPoints.contains(it) }
            .filter { heightMap[it.x][it.y] >= 0 }
            .toSet()
        queue.addAll(neighbors)
        foundPoints.add(point)

        return if (queue.isEmpty()) foundPoints
        else
            depthFirstSearch(
                queue.removeLast(),
                queue,
                foundPoints,
                boundingBox,
                heightMap
            )
    }

    fun solvePartTwo(input: List<String>): Int {
        val heightMap = input.map { it.toList().map { c -> if (c == '9') -1 else 0 } }
        val boundingBox = Point(heightMap.size, heightMap.maxOfOrNull { it.size } ?: 0)

        val allPoints = heightMap.indices
            .flatMap { row -> (0 until heightMap[0].size).map { col -> Point(row, col) } }
            .filter { heightMap[it.x][it.y] != -1 }

        val unexaminedPoints = allPoints.toMutableSet()

        return allPoints
            .fold(Triple(0, mutableListOf<Int>(), unexaminedPoints)) { acc, point ->
                if (!unexaminedPoints.contains(point))
                    acc
                else {
                    val basin = depthFirstSearch(point, ArrayDeque(allPoints.size), mutableSetOf(), boundingBox, heightMap)
                    acc.second.add(basin.size)
                    acc.third.removeAll(basin)
                    Triple(acc.first + 1, acc.second, acc.third)
                }
            }
            .second
            .sortedDescending()
            .take(3)
            .fold(1) { a, i -> a * i }
    }
}