package days

enum class Action(val label: String) {
    FORWARD("forward"),
    DOWN("down"),
    UP("up")
}

data class Command(val action: Action, val count: Int)
data class Position(val distance: Int, val depth: Int, val aim: Int)

fun commandFromInput(input: String): Command {
    val parts = input.split(" ")
    return Command(Action.values().first { it.label == parts[0] }, parts[1].toInt())
}

class Day02 : Day(2) {

    override fun partOne(): Int {
        return solvePartOne(inputList)
    }

    override fun partTwo(): Int {
        return solvePartTwo(inputList)
    }

    fun solvePartOne(input: List<String>): Int {
        val commands = input.map { commandFromInput(it) }
        val finalPosition = commands
            .fold(Position(0, 0, 0)) { pos, cmd ->
                when (cmd.action) {
                    Action.FORWARD -> Position(pos.distance + cmd.count, pos.depth, pos.aim)
                    Action.DOWN -> Position(pos.distance, pos.depth + cmd.count, pos.aim)
                    Action.UP -> Position(pos.distance, pos.depth - cmd.count, pos.aim)
                }
            }
        return finalPosition.distance * finalPosition.depth
    }

    fun solvePartTwo(input: List<String>): Int {
        val commands = input.map { commandFromInput(it) }
        val finalPosition = commands
            .fold(Position(0, 0, 0)) { pos, cmd ->
                when (cmd.action) {
                    Action.FORWARD -> Position(pos.distance + cmd.count, pos.depth + pos.aim * cmd.count, pos.aim)
                    Action.DOWN -> Position(pos.distance, pos.depth, pos.aim + cmd.count)
                    Action.UP -> Position(pos.distance, pos.depth, pos.aim - cmd.count)
                }
            }
        return finalPosition.distance * finalPosition.depth
    }

}