package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay20 {
    private val testInput = """
        ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

        #..#.
        #....
        ##..#
        ..#..
        ..###
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day20().solvePartOne(testInput), IsEqual.equalTo(35))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day20().partOne(), IsEqual.equalTo(5225))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day20().solvePartTwo(testInput), IsEqual.equalTo(3351))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day20().partTwo(), IsEqual.equalTo(18131))
    }
}