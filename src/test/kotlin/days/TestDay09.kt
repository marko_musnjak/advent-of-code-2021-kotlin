package days

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test

class TestDay09 {
    private val testInput = """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        assertThat(Day09().solvePartOne(testInput), equalTo(15))
    }

    @Test
    fun testPartOneWithRealInput() {
        assertThat(Day09().partOne(), equalTo(577))
    }

    @Test
    fun testPartTwo() {
        assertThat(Day09().solvePartTwo(testInput), equalTo(1134))
    }

    @Test
    fun testPartTwoWithRealInput() {
        assertThat(Day09().partTwo(), equalTo(1069200))
    }

}