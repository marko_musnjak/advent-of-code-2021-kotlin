package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay11 {
    private val testInput = """
        5483143223
        2745854711
        5264556173
        6141336146
        6357385478
        4167524645
        2176841721
        6882881134
        4846848554
        5283751526
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day11().solvePartOne(testInput), IsEqual.equalTo(1656))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day11().partOne(), IsEqual.equalTo(1591))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day11().solvePartTwo(testInput), IsEqual.equalTo(195))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day11().partTwo(), IsEqual.equalTo(314))
    }

}