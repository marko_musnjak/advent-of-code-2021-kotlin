package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay10 {
    private val testInput = """
        [({(<(())[]>[[{[]{<()<>>
        [(()[<>])]({[<{<<[]>>(
        {([(<{}[<>[]}>{[]{[(<()>
        (((({<>}<{<{<>}{[]{[]{}
        [[<[([]))<([[{}[[()]]]
        [{[{({}]{}}([{[{{{}}([]
        {<[[]]>}<{[{[{[]{()[[[]
        [<(<(<(<{}))><([]([]()
        <{([([[(<>()){}]>(<<{{
        <{([{{}}[<[[[<>{}]]]>[]]
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day10().solvePartOne(testInput), IsEqual.equalTo(26397))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day10().partOne(), IsEqual.equalTo(319233))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day10().solvePartTwo(testInput), IsEqual.equalTo(288957))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day10().partTwo(), IsEqual.equalTo(1118976874L))
    }

}