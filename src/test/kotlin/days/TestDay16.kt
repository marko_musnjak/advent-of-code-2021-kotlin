package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay16 {
    private val testInput = "A0016C880162017C3686B18A3D4780"

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day16().solvePartOne(testInput), IsEqual.equalTo(31))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day16().partOne(), IsEqual.equalTo(889))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day16().solvePartTwo(testInput), IsEqual.equalTo(54))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day16().partTwo(), IsEqual.equalTo(739303923668L))
    }
}