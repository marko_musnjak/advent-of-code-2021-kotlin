package days

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test

class TestDay01 {
    private val testInput = """
        199
        200
        208
        210
        200
        207
        240
        269
        260
        263
        """.trimIndent().split("\n").map { it.toInt() }

    @Test
    fun testPartOne() {
        assertThat(Day01().solvePartOne(testInput), equalTo(7))
    }

    @Test
    fun testPartOneWithRealInput() {
        assertThat(Day01().partOne(), equalTo(1215))
    }

    @Test
    fun testPartTwo() {
        assertThat(Day01().solvePartTwo(testInput), equalTo(5))
    }

    @Test
    fun testPartTwoWithRealInput() {
        assertThat(Day01().partTwo(), equalTo(1150))
    }
}
