package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay07 {
    private val testInput = """
        16,1,2,0,4,2,7,1,2,14
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day07().solvePartOne(testInput), IsEqual.equalTo(37))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day07().partOne(), IsEqual.equalTo(340056))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day07().solvePartTwo(testInput), IsEqual.equalTo(168))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day07().partTwo(), IsEqual.equalTo(96592275))
    }

}
