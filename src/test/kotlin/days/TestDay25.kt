package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay25 {
    private val testInput = """
        v...>>.vv>
        .vv>>.vv..
        >>.>v>...v
        >>v>>.>.v.
        v>v.vv.v..
        >.>>..v...
        .vv..>.>v.
        v.v..>>v.v
        ....v..v.>
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day25().solvePartOne(testInput), IsEqual.equalTo(58))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day25().partOne(), IsEqual.equalTo(-1))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day25().solvePartTwo(testInput), IsEqual.equalTo(-1))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day25().partTwo(), IsEqual.equalTo(-1))
    }
}