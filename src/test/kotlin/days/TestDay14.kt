package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay14 {
    private val testInput = """
        NNCB

        CH -> B
        HH -> N
        CB -> H
        NH -> C
        HB -> C
        HC -> B
        HN -> C
        NN -> C
        BH -> H
        NC -> B
        NB -> B
        BN -> B
        BB -> N
        BC -> B
        CC -> N
        CN -> C
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day14().solvePartOne(testInput), IsEqual.equalTo(1588))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day14().partOne(), IsEqual.equalTo(3555))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day14().solvePartTwo(testInput), IsEqual.equalTo(2188189693529L))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day14().partTwo(), IsEqual.equalTo(4439442043739L))
    }
}