package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay21 {
    private val testInput = """
        Player 1 starting position: 4
        Player 2 starting position: 8
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day21().solvePartOne(testInput), IsEqual.equalTo(739785))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day21().partOne(), IsEqual.equalTo(711480))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day21().solvePartTwo(testInput), IsEqual.equalTo(444356092776315))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day21().partTwo(), IsEqual.equalTo(265845890886828L))
    }
}