package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class TestDay15 {
    private val testInput = """
        1163751742
        1381373672
        2136511328
        3694931569
        7463417111
        1319128137
        1359912421
        3125421639
        1293138521
        2311944581
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day15().solvePartOne(testInput), IsEqual.equalTo(40))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day15().partOne(), IsEqual.equalTo(435))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day15().solvePartTwo(testInput), IsEqual.equalTo(315))
    }

    @Test @Disabled
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day15().partTwo(), IsEqual.equalTo(2842))
    }
}