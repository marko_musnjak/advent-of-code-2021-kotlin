package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class TestDay23 {
    private val testInput = """
        """.trimIndent().split("\n")

    @Disabled @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day23().solvePartOne(testInput), IsEqual.equalTo(-1))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day23().partOne(), IsEqual.equalTo(1851))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day23().solvePartTwo(testInput), IsEqual.equalTo(-1))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day23().partTwo(), IsEqual.equalTo(-1))
    }
}