package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay12 {
    private val testInput = """
        start-A
        start-b
        A-c
        A-b
        b-d
        A-end
        b-end
        """.trimIndent().split("\n")

    private val largerTestInput = """
        dc-end
        HN-start
        start-kj
        dc-start
        dc-HN
        LN-dc
        HN-end
        kj-sa
        kj-HN
        kj-dc
        """.trimIndent().split("\n")

    private val largestTestInput = """
        fs-end
        he-DX
        fs-he
        start-DX
        pj-DX
        end-zg
        zg-sl
        zg-pj
        pj-he
        RW-he
        fs-DX
        pj-RW
        zg-RW
        start-pj
        he-WI
        zg-he
        pj-fs
        start-RW
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day12().solvePartOne(testInput), IsEqual.equalTo(10))
    }

    @Test
    fun testPartOneWithLargerExample() {
        MatcherAssert.assertThat(Day12().solvePartOne(largerTestInput), IsEqual.equalTo(19))
    }

    @Test
    fun testPartOneWithLargestExample() {
        MatcherAssert.assertThat(Day12().solvePartOne(largestTestInput), IsEqual.equalTo(226))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day12().partOne(), IsEqual.equalTo(3000))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day12().solvePartTwo(testInput), IsEqual.equalTo(36))
    }

    @Test
    fun testPartTwoWithLargerExample() {
        MatcherAssert.assertThat(Day12().solvePartTwo(largerTestInput), IsEqual.equalTo(103))
    }

    @Test
    fun testPartTwoWithLargestExample() {
        MatcherAssert.assertThat(Day12().solvePartTwo(largestTestInput), IsEqual.equalTo(3509))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day12().partTwo(), IsEqual.equalTo(74222))
    }
}