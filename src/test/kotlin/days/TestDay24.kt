package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class TestDay24 {
    private val testInput = """
        """.trimIndent().split("\n")

    @Disabled
    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day24().solvePartOne(testInput), IsEqual.equalTo(-1))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day24().partOne(), IsEqual.equalTo(69914999975369))
    }

    @Disabled
    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day24().solvePartTwo(testInput), IsEqual.equalTo(-1))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day24().partTwo(), IsEqual.equalTo(-1))
    }
}