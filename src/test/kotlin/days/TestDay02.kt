package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay02 {
    private val testInput = """
        forward 5
        down 5
        forward 8
        up 3
        down 8
        forward 2
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day02().solvePartOne(testInput), IsEqual.equalTo(150))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day02().partOne(), IsEqual.equalTo(1840243))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day02().solvePartTwo(testInput), IsEqual.equalTo(900))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day02().partTwo(), IsEqual.equalTo(1727785422))
    }
}