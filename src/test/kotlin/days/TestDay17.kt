package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay17 {
    private val testInput = "target area: x=20..30, y=-10..-5"

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day17().solvePartOne(testInput), IsEqual.equalTo(45))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day17().partOne(), IsEqual.equalTo(7503))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day17().solvePartTwo(testInput), IsEqual.equalTo(112))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day17().partTwo(), IsEqual.equalTo(3229))
    }
}