package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay06 {
    private val testInput = "3,4,3,1,2"

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day06().solvePartOne(testInput), IsEqual.equalTo(5934L))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day06().partOne(), IsEqual.equalTo(373378L))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day06().solvePartTwo(testInput), IsEqual.equalTo(26984457539L))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day06().partTwo(), IsEqual.equalTo(1682576647495L))
    }

}