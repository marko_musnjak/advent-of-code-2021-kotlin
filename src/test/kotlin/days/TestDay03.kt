package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay03 {
    private val testInput = """
        00100
        11110
        10110
        10111
        10101
        01111
        00111
        11100
        10000
        11001
        00010
        01010
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day03().solvePartOne(testInput), IsEqual.equalTo(198))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day03().partOne(), IsEqual.equalTo(2583164))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day03().solvePartTwo(testInput), IsEqual.equalTo(230))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day03().partTwo(), IsEqual.equalTo(2784375))
    }
}