package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay05 {
    private val testInput = """
        0,9 -> 5,9
        8,0 -> 0,8
        9,4 -> 3,4
        2,2 -> 2,1
        7,0 -> 7,4
        6,4 -> 2,0
        0,9 -> 2,9
        3,4 -> 1,4
        0,0 -> 8,8
        5,5 -> 8,2
        """.trimIndent().split("\n")

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day05().solvePartOne(testInput), IsEqual.equalTo(5))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day05().partOne(), IsEqual.equalTo(6113))
    }

    @Test
    fun testPartTwo() {
        MatcherAssert.assertThat(Day05().solvePartTwo(testInput), IsEqual.equalTo(12))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day05().partTwo(), IsEqual.equalTo(20373))
    }

}