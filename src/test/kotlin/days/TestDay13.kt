package days

import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsEqual
import org.junit.jupiter.api.Test

class TestDay13 {
    private val testInput = """
        6,10
        0,14
        9,10
        0,3
        10,4
        4,11
        6,0
        6,12
        4,1
        0,13
        10,12
        3,4
        3,0
        8,4
        1,10
        2,14
        8,10
        9,0

        fold along y=7
        fold along x=5
        """.trimIndent()

    @Test
    fun testPartOne() {
        MatcherAssert.assertThat(Day13().solvePartOne(testInput), IsEqual.equalTo(17))
    }

    @Test
    fun testPartOneWithRealInput() {
        MatcherAssert.assertThat(Day13().partOne(), IsEqual.equalTo(653))
    }

    @Test
    fun testPartTwo() {
         MatcherAssert.assertThat(Day13().solvePartTwo(testInput), IsEqual.equalTo(16))
    }

    @Test
    fun testPartTwoWithRealInput() {
        MatcherAssert.assertThat(Day13().partTwo(), IsEqual.equalTo(102))
    }
}